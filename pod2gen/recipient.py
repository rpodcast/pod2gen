# -*- coding: utf-8 -*-
"""
    pod2gen.recipient
    ~~~~~~~~~~~~~~~~~

    This file contains the Recipient class, which is used to represent a
    recipient inside a value block.
"""
import warnings

from lxml import etree


class Recipient(object):
    def __init__(
        self,
        address_type,
        address,
        split,
        name=None,
        customKey=None,
        customValue=None,
        fee=None
    ):
        """Create new recipient"""
        if not self._is_valid(address_type, address, split):
            raise ValueError("You must provide an address type, address, and split.")

        self.__address_type = address_type
        self.__address = address
        self.__name = None
        self.__split = None
        self.__customKey = None
        self.__customValue = None
        self.__fee = None

        self.name = name
        self.split = split
        self.customKey = customKey
        self.customValue = customValue
        self.fee = fee

    def _is_valid(self, address_type, address, split):
        return address_type or address or split

    def rss_entry(self):
        PODCAST_NS = "https://podcastindex.org/namespace/1.0"
        entry = etree.Element("{%s}valueRecipient" % PODCAST_NS)

        if self.name:
            entry.attrib["name"] = self.name

        entry.attrib["type"] = self.address_type
        entry.attrib["address"] = self.address
        entry.attrib["split"] = self.split

        if self.customKey:
            entry.attrib["customKey"] = self.customKey

        if self.customValue:
            entry.attrib["customValue"] = self.customValue

        if self.fee is not None:
            if self.fee:
                entry.attrib["fee"] = "true"
            else:
                entry.attrib["fee"] = "false"

        return entry

    @property
    def name(self):
        """This recipient's name.

        :type: :obj:`str`
        """

        return self.__name

    @name.setter
    def name(self, new_name):
        self.__name = new_name

    @property
    def address_type(self):
        """This recipient's type of receiving address

        :type: :obj:`str`
        """
        return self.__address_type

    @address_type.setter
    def address_type(self, new_type):
        self.__address_type = new_type

    @property
    def address(self):
        """This recipient's address that will receive the payment

        :type: :obj:`str`
        """
        return self.__address

    @address.setter
    def address(self, new_address):
        self.__address = new_address

    @property
    def split(self):
        """The number of shares of the payment this recipient will receive

        :type: :obj:`int`
        """
        return str(self.__split)

    @split.setter
    def split(self, new_split):
        self.__split = new_split

    @property
    def customKey(self):
        """Optional name of a custom record key to send along with the payment

        :type: :obj:`str`
        """
        return self.__customKey

    @customKey.setter
    def customKey(self, new_customKey):
        self.__customKey = new_customKey

    @property
    def customValue(self):
        """Optional custom value to pass along with the payment. This is considered
        the value that belongs to the customKey.

        :type: :obj:`str`
        """
        return self.__customValue

    @customValue.setter
    def customValue(self, new_customValue):
        self.__customValue = new_customValue

    @property
    def fee(self):
        """Optional boolean indicating this is a transaction fee

        :type: :obj:`bool`
        """
        return self.__fee

    @fee.setter
    def fee(self, new_fee):
        if new_fee is not None:
            if isinstance(new_fee, bool):
                self.__fee = new_fee
            else:
                raise ValueError("fee attribute must be a boolean")
        else:
            self.__fee = None

    def __str__(self):
        return "%s %s" % (self.address, self.address_type, self.split)

    def __repr__(self):
        return "Recipient(address=%s, address_type=%s, split=%s)" % (
            self.address,
            self.address_type,
            self.split
        )
