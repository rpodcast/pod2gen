# -*- coding: utf-8 -*-
"""
    pod2gen.value
    ~~~~~~~~~~~~~~~~~

    This file contains the Value class, which is used to represent a
    value block.
"""
import warnings

from lxml import etree

from .recipient import Recipient


class Value(object):
    def __init__(self, value_type=None, method=None, suggested=None, recipients=None, timesplits=None):
        """Create new value"""
        if not self._is_valid(value_type, method):
            raise ValueError("You must provide a value type and method.")

        self.__value_type = value_type
        self.__method = method
        self.__suggested = None
        self.__recipients = recipients
        self.__timesplits = timesplits

        self.suggested = suggested

    def rss_entry(self):
        PODCAST_NS = "https://podcastindex.org/namespace/1.0"
        entry = etree.Element("{%s}value" % PODCAST_NS)

        if self.suggested:
            entry.attrib["suggested"] = self.suggested

        entry.attrib["type"] = self.value_type
        entry.attrib["method"] = self.method

        if self.__recipients:
            for recipient_item in self.__recipients:
                recipient_block = recipient_item.rss_entry()
                entry.append(recipient_block)

        if self.__timesplits:
            for timesplit_item in self.__timesplits:
                timesplit_block = timesplit_item.rss_entry()
                entry.append(timesplit_block)
        
        return entry

    def _is_valid(self, value_type, method):
        """Check whether one of value_type and method are usable."""
        return value_type or method

    @property
    def value_type(self):
        """This value's type service slug

        :type: :obj:`str`
        """
        return self.__value_type

    @value_type.setter
    def value_type(self, new_type):
        self.__value_type = new_type

    @property
    def method(self):
        """This value's transport mechanism

        :type: :obj:`str`
        """
        return self.__method

    @method.setter
    def address(self, new_method):
        self.__method = new_method

    @property
    def suggested(self):
        """Optional suggestion on how much cryptocurrency to send with each payment

        :type: :obj:`str`
        """
        return self.__suggested

    @suggested.setter
    def suggested(self, new_suggested):
        self.__suggested = new_suggested

    @property
    def recipients(self):
        return self.__recipients

    @recipients.setter
    def recipients(self, recipients):
        if recipients is not None:
            recipients = []

        try:
            recipients = list(recipients)
        except:
            raise TypeError("Expected an iterable of Recipient, got %r" % recipients)

        for recipient_item in recipients:
            if not isinstance(recipient_item, Recipient):
                raise TypeError(
                    "A Recipient object must be used, got " "%r" % recipient_item
                )

        self.__recipients = recipients

    @property
    def timesplits(self):
        return self.__timesplits

    @timesplits.setter
    def timesplits(self, timesplits):
        if timesplits is not None:
            timesplits = []

        try:
            timesplits = list(timesplits)
        except:
            raise TypeError("Expected an iterable of TimeSplit, got %r" % timesplits)
        
        for timesplit_item in timesplits:
            if not isinstance(timesplit_item, TimeSplit):
                raise TypeError(
                    "A TimeSplit object must be used, got " "%r" % timesplit_item
                )
        
        self.__timesplits = timesplits

    def __str__(self):
        return "%s %s" % (self.value_type, self.method)

    def __repr__(self):
        return "Value(value_type=%s, method=%s)" % (self.value_type, self.method)
