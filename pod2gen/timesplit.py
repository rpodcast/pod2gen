# -*- coding: utf-8 -*-
"""
    pod2gen.timesplit
    ~~~~~~~~~~~~~~~~~

    This file contains the TimeSplit class, which is used to represent a
    value time split inside a valueTimeSplit block.
"""
import warnings

from lxml import etree

from .recipient import Recipient
from .remoteitem import RemoteItem


class TimeSplit(object):
    def __init__(
        self,
        startTime,
        duration,
        remoteStartTime=None,
        remotePercentage=None,
        recipients=None,
        remoteitems = None
    ):
        """Create new timesplit"""
        if not self._is_valid(startTime, duration):
            raise ValueError("You must provide a startTime and duration.")

        self.__startTime = startTime
        self.__duration = duration
        self.__remoteStartTime = remoteStartTime
        self.__remotePercentage = remotePercentage
        self.__recipients = recipients
        self.__remoteitems = remoteitems

    def _is_valid(self, startTime, duration):
        return startTime or duration
    
    def rss_entry(self):
        PODCAST_NS = "https://podcastindex.org/namespace/1.0"
        entry = etree.Element("{%s}valueTimeSplit" % PODCAST_NS)

        entry.attrib["startTime"] = self.startTime
        entry.attrib["duration"] = self.duration

        if self.__remoteStartTime:
            entry.attrib["remoteStartTime"] = self.remoteStartTime
        
        if self.__remotePercentage:
            entry.attrib["remotePercentage"] = self.remotePercentage

        if self.__recipients:
            for recipient_item in self.__recipients:
                recipient_block = recipient_item.rss_entry()
                entry.append(recipient_block)

        if self.__remoteitems:
            for remoteitem_item in self.__remoteitems:
                remoteitem_block = remoteitem_item.rss_entry()
                entry.append(remoteitem_block)

        return entry

    @property
    def startTime(self):
        """This time split's start time in seconds
        
        :type: :obj:`int`
        """
        return str(self.__startTime)

    @startTime.setter
    def startTime(self, new_startTime):
        self.__startTime = new_startTime

    @property
    def duration(self):
        """How many seconds to use this element's 
        value recipient information before switching
        back to value recipient information of
        parent feed
        
        :type: :obj"`int`
        """
        return str(self.__duration)

    @duration.setter
    def duration(self, new_duration):
        self.__duration = new_duration
    
    @property
    def remoteStartTime(self):
        """The remote item's start time in seconds
        for the value split
        
        :type: :obj:`int`
        """
        return str(self.__remoteStartTime)
    
    @remoteStartTime.setter
    def remoteStartTime(self, new_remoteStartTime):
        self.__remoteStartTime = new_remoteStartTime
    
    @property
    def remotePercentage(self):
        """Percentage of payment the remote recipient
        will receive if a remote item is present
        
        :type: :obj:`int`
        """
        return str(self.__remotePercentage)

    @remotePercentage.setter
    def remotePercentage(self, new_remotePercentage):
        self.__remotePercentage = new_remotePercentage
    
    def __str__(self):
        return "%s %s" % (self.startTime, self.duration)

    def __repr__(self):
        return "TimeSplit(startTime=%s, duration=%s)" % (
            self.startTime,
            self.duration
        )
    
    @property
    def recipients(self):
        return self.__recipients

    @recipients.setter
    def recipients(self, recipients):
        if recipients is not None:
            recipients = []

        try:
            recipients = list(recipients)
        except:
            raise TypeError("Expected an iterable of Recipient, got %r" % recipients)

        for recipient_item in recipients:
            if not isinstance(recipient_item, Recipient):
                raise TypeError(
                    "A Recipient object must be used, got " "%r" % recipient_item
                )

        self.__recipients = recipients

    @property
    def remoteitems(self):
        return self.__remoteitems

    @remoteitems.setter
    def remoteitems(self, remoteitems):
        if remoteitems is not None:
            remoteitems = []

        try:
            remoteitems = list(remoteitems)
        except:
            raise TypeError("Expected an iterable of RemoteItem, got %r" % remoteitems)

        for remoteitem_item in remoteitems:
            if not isinstance(remoteitem_item, RemoteItem):
                raise TypeError(
                    "A RemoteItem object must be used, got " "%r" % remoteitem_item
                )

        self.__remoteitems = remoteitems

    def __str__(self):
        return "%s %s" % (self.startTime, self.duration)

    def __repr__(self):
        return "TimeSplit(startTime=%s, duration=%s)" % (self.startTime, self.duration)
