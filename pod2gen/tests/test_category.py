# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_category
    ~~~~~~~~~~~~~~~~~~~~~~~~~~

    Module for testing the Category class.

    :copyright: 2016, Thorben Dahl <thorben@sjostrom.no>
        2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest
import warnings

from pod2gen import Category, LegacyCategoryWarning


class TestCategory(unittest.TestCase):
    def test_constructorWithSubcategory(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always", LegacyCategoryWarning)

            c = Category("Arts", "Food")
            self.assertEqual(c.category, "Arts")
            self.assertEqual(c.subcategory, "Food")

            # No warning should be given
            self.assertEqual(len(w), 0)

    def test_constructorWithoutSubcategory(self):
        c = Category("Arts")
        self.assertEqual(c.category, "Arts")
        self.assertTrue(c.subcategory is None)

    def test_constructorInvalidCategory(self):
        self.assertRaises(ValueError, Category, "Farts", "Food")

    def test_constructorInvalidSubcategory(self):
        self.assertRaises(ValueError, Category, "Arts", "Flood")

    def test_constructorSubcategoryWithoutCategory(self):
        self.assertRaises((ValueError, TypeError), Category, None, "Food")

    def test_constructorCaseInsensitive(self):
        c = Category("arTS", "FOOD")
        self.assertEqual(c.category, "Arts")
        self.assertEqual(c.subcategory, "Food")

    def test_immutable(self):
        c = Category("Arts", "Food")
        self.assertRaises(AttributeError, setattr, c, "category", "Fiction")
        self.assertEqual(c.category, "Arts")

        self.assertRaises(AttributeError, setattr, c, "subcategory", "Science Fiction")
        self.assertEqual(c.subcategory, "Food")

    def test_escapedIsAccepted(self):
        c = Category("Kids &amp; Family", "Pets &amp; Animals")
        self.assertEqual(c.category, "Kids & Family")
        self.assertEqual(c.subcategory, "Pets & Animals")

    def test_oldCategoryIsAcceptedWithWarning(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always", LegacyCategoryWarning)

            c = Category("Government & Organizations")
            self.assertEqual(c.category, "Government & Organizations")

            self.assertEqual(len(w), 1)
            self.assertIsInstance(w[0].message, LegacyCategoryWarning)

    def test_oldSubcategoryIsAcceptedWithWarnings(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always", LegacyCategoryWarning)

            c = Category("Technology", "Podcasting")
            self.assertEqual(c.category, "Technology")
            self.assertEqual(c.subcategory, "Podcasting")

            self.assertEqual(len(w), 1)
            self.assertIsInstance(w[0].message, LegacyCategoryWarning)

    def test_oldCategorySubcategoryIsAcceptedWithWarnings(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always", LegacyCategoryWarning)

            c = Category("Science & Medicine", "Medicine")
            self.assertEqual(c.category, "Science & Medicine")
            self.assertEqual(c.subcategory, "Medicine")

            self.assertEqual(len(w), 1)
            self.assertIsInstance(w[0].message, LegacyCategoryWarning)
