# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_documentation
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Sphinx documentation is being generated properly

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import shutil
import unittest

from sphinx.application import Sphinx


class TestDocumentation(unittest.TestCase):
    def setUp(self):
        self.source_dir = "doc"
        self.config_dir = "doc"
        self.output_dir = "docs/build"
        self.doctree_dir = "docs/build/doctrees"
        self.all_files = 1

    def test_html_documentation(self):
        try:
            shutil.rmtree("docs", ignore_errors=False, onerror=None)
        except:
            pass

        app = Sphinx(
            self.source_dir,
            self.config_dir,
            self.output_dir,
            self.doctree_dir,
            buildername="html",
            warningiserror=True,
        )
        app.build(force_all=self.all_files)
