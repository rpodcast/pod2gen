# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_person
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Transcript class, which represents an episode transcript.

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest

from pod2gen import NotSupportedByItunesWarning, Transcript


class TestTranscript(unittest.TestCase):
    def setUp(self):
        self.url = "https://examples.com/transcript_sample.txt"
        self.type = "text/html"
        self.language = "es-US"
        self.is_caption = True

    def _transcript(self):
        return Transcript(
            self.url, self.type, language=self.language, is_caption=self.is_caption
        )

    def test_OnlyUrlAndType(self):
        t = Transcript(self.url, self.type)
        assert t.url == self.url
        assert t.type == self.type
        assert t.language is None
        assert t.is_caption is False

    def test_isCaption(self):
        t = Transcript(self.url, self.type, is_caption=True)
        assert t.url == self.url
        assert t.type == self.type
        assert t.language is None
        assert t.is_caption is True

    def test_languageCode(self):
        t = Transcript(self.url, self.type, language=self.language)
        assert t.url == self.url
        assert t.type == self.type
        assert t.language == self.language
        assert t.is_caption is False

    def test_languageAndIsCaption(self):
        t = Transcript(
            self.url, self.type, is_caption=self.is_caption, language=self.language
        )
        assert t.url == self.url
        assert t.type == self.type
        assert t.language == self.language
        assert t.is_caption == self.is_caption

    def test_settingUrl(self):
        other_url = "https://examples.com/transcript_sample_2.txt"
        t = self._transcript()
        t.url = other_url
        assert t.url == other_url
        assert t.type == self.type
        assert t.language == self.language
        assert t.is_caption == self.is_caption

    def test_settingType(self):
        other_type = "application/json"
        t = self._transcript()
        t.type = other_type
        assert t.url == self.url
        assert t.type == other_type
        assert t.language == self.language
        assert t.is_caption == self.is_caption

    def test_settingLanguageCode(self):
        other_language = "fr-CA"
        t = self._transcript()
        t.language = other_language
        assert t.url == self.url
        assert t.type == self.type
        assert t.language == other_language
        assert t.is_caption == self.is_caption

        t.language = None
        assert t.url == self.url
        assert t.type == self.type
        assert t.language == None
        assert t.is_caption == self.is_caption

    def test_languageCaseInsensitive(self):
        t = self._transcript()
        t.language = "fR-cA"
        assert t.language == "fr-CA"

    def test_settingIsCaption(self):
        other_caption_option = False
        t = self._transcript()
        t.is_caption = other_caption_option
        assert t.url == self.url
        assert t.type == self.type
        assert t.language == self.language
        assert t.is_caption == other_caption_option

        old_caption_option = t.is_caption
        t.is_caption = not old_caption_option
        assert t.url == self.url
        assert t.type == self.type
        assert t.language == self.language
        assert t.is_caption != old_caption_option

    def test_missingUrl(self):
        self.assertRaises(ValueError, Transcript, None, self.type)

    def test_warningMissingHttp(self):
        missing_http = "ftp://examples.com/transcript_sample_2.txt"
        t = self._transcript()
        t.url = missing_http
        self.assertWarns(NotSupportedByItunesWarning, setattr, t, "url", missing_http)

    def test_wrongUrl(self):
        self.assertRaises(ValueError, Transcript, "Not an actual url", self.type)

    def test_missingType(self):
        self.assertRaises(ValueError, Transcript, self.url, None)

    def test_wrongType(self):
        self.assertRaises(ValueError, Transcript, self.url, "Wrong type")

    def test_wrongLanguage(self):
        self.assertRaises(ValueError, Transcript, self.url, self.type, "aaa")
        self.assertRaises(ValueError, Transcript, self.url, self.type, "en-aaa")

    def test_wronCaptionOption(self):
        self.assertRaises(
            ValueError, Transcript, self.url, self.type, is_caption="Not a boolean"
        )
