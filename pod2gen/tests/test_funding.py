# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_funding
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Funding class, which represents a donation/funding link.

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest

from pod2gen import Funding


class TestFunding(unittest.TestCase):
    def setUp(self):
        self.text = "Please support me!"
        self.url = "https://examples.com/donation_link.html"

    def _funding(self):
        return Funding(self.text, self.url)

    def test_constructor(self):
        f = Funding(self.text, self.url)
        assert f.text == self.text
        assert f.url == self.url

    def test_constructorMissingParameter(self):
        self.assertRaises(TypeError, Funding, "param_1")

    def test_settingText(self):
        other_text = "Become a member!"
        f = self._funding()
        f.text = other_text
        assert f.text == other_text
        assert f.url == self.url

    def test_wrongText(self):
        self.assertRaises(ValueError, Funding, 0, self.url)
        self.assertRaises(ValueError, Funding, None, self.url)
        self.assertRaises(ValueError, Funding, "", self.url)
        self.assertRaises(ValueError, Funding, "0" * 129, self.url)

    def test_settingUrl(self):
        other_url = "https://examples.com/donation_link2.html"
        f = self._funding()
        f.url = other_url
        assert f.text == self.text
        assert f.url == other_url

    def test_wrongUrl(self):
        self.assertRaises(ValueError, Funding, self.text, "")
        self.assertRaises(ValueError, Funding, self.text, None)
        self.assertRaises(ValueError, Funding, self.text, "Not an actual url")
