# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_alternate_media
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the AlternateMedia class, which is a representation of the
     <alternateEnclosure> tag.

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest

from pod2gen import AlternateMedia


class TestAlternateMedia(unittest.TestCase):
    PODCAST_NS = "https://podcastindex.org/namespace/1.0"

    def setUp(self):
        self.type = "audio/mp4"
        self.length = 43200000
        self.bitrate = 128000
        self.height = 1080
        self.lang = "en-US"
        self.title = "Standard"
        self.rel = "Off stage"
        self.codecs = "mp4a.40.2"
        self.default = False
        self.default_str = "false"
        self.encryption = "sri"
        self.signature = (
            "sha384-ExVqijgYHm15PqQqdXfW95x+Rs6C+d6E/ICxyQOeFevnxNLR/wtJNrNYTjIysUBo"
        )
        self.sources = {
            "https://example.com/file-0.mp3": None,
            "ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y": None,
            "https://example.com/file-0.torrent": "application/x-bittorrent",
        }

    def _alternate_media(self):
        m = AlternateMedia(
            self.type,
            self.length,
            bitrate=self.bitrate,
            height=self.height,
            lang=self.lang,
            title=self.title,
            rel=self.rel,
            codecs=self.codecs,
            default=self.default,
            encryption=self.encryption,
            signature=self.signature,
        )
        m.sources = self.sources
        return m

    def test_constructorWithNoKwargs(self):
        m = AlternateMedia(self.type, self.length)
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate is None
        assert m.height is None
        assert m.lang is None
        assert m.title is None
        assert m.rel is None
        assert m.codecs is None
        assert m.default is None
        assert m.encryption is None
        assert m.signature is None

    def test_constructorKwargs(self):
        m = self._alternate_media()
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

    def test_constructorMissingParameter(self):
        self.assertRaises(TypeError, AlternateMedia, "param_1")

    def test_settingType(self):
        other_type = "video/mp4"
        m = self._alternate_media()
        m.type = other_type
        assert other_type != self.type
        assert m.type == other_type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

    def test_wrongType(self):
        m = self._alternate_media()
        self.assertRaises(TypeError, setattr, m, "type", 55)
        self.assertRaises(ValueError, setattr, m, "type", None)
        self.assertRaises(ValueError, setattr, m, "type", "")

    def test_settingLength(self):
        other_length = 23400000
        m = self._alternate_media()
        m.length = other_length
        assert other_length != self.length
        assert m.type == self.type
        assert m.length == other_length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

    def test_wrongLength(self):
        m = self._alternate_media()
        self.assertRaises(TypeError, setattr, m, "length", "not integer")
        self.assertRaises(ValueError, setattr, m, "length", None)
        self.assertRaises(ValueError, setattr, m, "length", 0)

    def test_settingBitrate(self):
        other_bitrate = 256000
        m = self._alternate_media()
        m.bitrate = other_bitrate
        assert other_bitrate != self.bitrate
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == other_bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.bitrate = None
        assert m.bitrate is None

    def test_wrongBitrate(self):
        m = self._alternate_media()
        self.assertRaises(TypeError, setattr, m, "bitrate", "not inetger")

    def test_settingHeight(self):
        other_height = 360
        m = self._alternate_media()
        m.height = other_height
        assert other_height != self.height
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == other_height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.height = None
        assert m.height is None

    def test_wrongHeight(self):
        m = self._alternate_media()
        self.assertRaises(TypeError, setattr, m, "height", "not float")

    def test_settingLanguage(self):
        other_language = "fr-CA"
        m = self._alternate_media()
        m.lang = other_language
        assert other_language != self.lang
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == other_language
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.title = None
        assert m.title is None

    def test_wrongLanguage(self):
        m = self._alternate_media()
        self.assertRaises(ValueError, setattr, m, "lang", "aaa")
        self.assertRaises(ValueError, setattr, m, "lang", "en-aaa")

        m.lang = 0
        assert m.lang is None

    def test_settingTitle(self):
        other_title = "Making of"
        m = self._alternate_media()
        m.title = other_title
        assert other_title != self.title
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == other_title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.title = None
        assert m.title is None

    def test_wrongTitle(self):
        m = self._alternate_media()
        self.assertRaises(ValueError, setattr, m, "title", "a" * 33)

        m.title = 0
        assert m.title is None

    def test_settingRel(self):
        other_rel = "Making of"
        m = self._alternate_media()
        m.rel = other_rel
        assert other_rel != self.rel
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == other_rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.rel = None
        assert m.rel is None

    def test_wrongRel(self):
        m = self._alternate_media()
        self.assertRaises(ValueError, setattr, m, "rel", "a" * 33)

        m.rel = 0
        assert m.rel is None

    def test_settingCodecs(self):
        other_codecs = "mp4a.40.3"
        m = self._alternate_media()
        m.codecs = other_codecs
        assert other_codecs != self.codecs
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == other_codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.codecs = None
        assert m.codecs is None

    def test_wrongCodecs(self):
        m = self._alternate_media()
        o = object
        self.assertRaises(ValueError, setattr, m, "codecs", o)

        m.codecs = 0
        assert m.codecs is None

    def test_settingDefault(self):
        other_default = True
        m = self._alternate_media()
        m.default = other_default
        assert other_default != self.default
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == other_default
        assert m.encryption == self.encryption
        assert m.signature == self.signature

        m.default = None
        assert m.default is None

    def test_wrongDefault(self):
        m = self._alternate_media()
        self.assertRaises(ValueError, setattr, m, "default", object)
        self.assertRaises(ValueError, setattr, m, "default", "a string")
        self.assertRaises(ValueError, setattr, m, "default", "")
        self.assertRaises(ValueError, setattr, m, "default", 5)

    def test_settingEncryption(self):
        other_encyption = "pgp-signature"
        m = self._alternate_media()
        m.encryption = other_encyption
        assert other_encyption != self.encryption
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == other_encyption
        assert m.signature == self.signature

        m.encryption = None
        assert m.encryption is None

    def test_wrongEncyption(self):
        m = self._alternate_media()
        self.assertRaises(ValueError, setattr, m, "encryption", object)
        self.assertRaises(ValueError, setattr, m, "encryption", "a string")
        self.assertRaises(ValueError, setattr, m, "encryption", "")
        self.assertRaises(ValueError, setattr, m, "encryption", 5)
        self.assertRaises(ValueError, setattr, m, "encryption", True)

    def test_settingSignature(self):
        other_signature = (
            "sha384-ExVqijgYHm15PqQqdXfW95x+Rs6C+d6E/ICxyQOeFevnxNLR/wtJNrNYTjIysUBx"
        )
        m = self._alternate_media()
        m.signature = other_signature
        assert other_signature != self.signature
        assert m.type == self.type
        assert m.length == self.length
        assert m.bitrate == self.bitrate
        assert m.height == self.height
        assert m.lang == self.lang
        assert m.title == self.title
        assert m.rel == self.rel
        assert m.codecs == self.codecs
        assert m.default == self.default
        assert m.encryption == self.encryption
        assert m.signature == other_signature

        m.signature = None
        assert m.signature is None

    def test_wrongSignature(self):
        m = self._alternate_media()
        self.assertRaises(ValueError, setattr, m, "encryption", object)
        self.assertRaises(ValueError, setattr, m, "encryption", 5)
        self.assertRaises(ValueError, setattr, m, "encryption", True)

    def test_addingSource(self):
        m = AlternateMedia(type=self.type, length=self.length)
        assert len(m.sources) == 0

        source_1 = "https://example.com/file-0.mp3"
        m.add_source(source_1)
        assert len(m.sources) == 1
        assert source_1 in m.sources
        assert m.sources[source_1] is None

        source_2 = "ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y"
        m.add_source(source_2)
        assert len(m.sources) == 2
        assert source_2 in m.sources
        assert m.sources[source_2] is None

        source_3 = "https://example.com/file-0.torrent"
        m.add_source(source_3, "application/x-bittorrent")
        assert len(m.sources) == 3
        assert source_3 in m.sources
        assert m.sources[source_3] == "application/x-bittorrent"

    def test_addingSourceError(self):
        m = AlternateMedia(type=self.type, length=self.length)
        self.assertRaises(TypeError, m.add_source, object)
        self.assertRaises(TypeError, m.add_source, None)
        self.assertRaises(TypeError, m.add_source, 0)
        self.assertRaises(ValueError, m.add_source, "")
        self.assertRaises(ValueError, m.add_source, "not an actual uri")

        source = "https://example.com/file-0.mp3"
        m.add_source(source)
        self.assertRaises(ValueError, m.add_source, source)

        another_source = "ipfs://someRandomProprietaryAACFile"
        self.assertRaises(TypeError, m.add_source, another_source, object)
        self.assertRaises(TypeError, m.add_source, another_source, 0)

    def test_deletingSource(self):
        m = AlternateMedia(type=self.type, length=self.length)
        source_1 = "https://example.com/file-0.mp3"
        m.add_source(source_1)
        source_2 = "ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y"
        m.add_source(source_2)
        assert len(m.sources) == 2

        undefined_source = "https://example.com/not_in_sources.mp3"
        m.delete_source(undefined_source)
        assert len(m.sources) == 2

        m.delete_source(source_1)
        assert len(m.sources) == 1

        m.delete_source(source_2)
        assert len(m.sources) == 0
        assert m.sources == {}

    def test_editingContentType(self):
        m = AlternateMedia(type=self.type, length=self.length)
        source_1 = "https://example.com/file-0.mp3"
        m.add_source(source_1)
        source_2 = "ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y"
        m.add_source(source_2)
        assert len(m.sources) == 2

        assert m.sources[source_1] is None
        assert m.sources[source_2] is None

        m.edit_source_content(source_1, "application/x-bittorrent")
        assert m.sources[source_1] == "application/x-bittorrent"
        assert m.sources[source_2] is None

    def test_editingContentTypeError(self):
        m = AlternateMedia(type=self.type, length=self.length)
        source_1 = "https://example.com/file-0.mp3"
        m.add_source(source_1)
        assert len(m.sources) == 1

        self.assertRaises(TypeError, m.edit_source_content, source_1, type=0)
        self.assertRaises(TypeError, m.edit_source_content, source_1, type=object)

        undefined_source = "https://example.com/not_in_sources.mp3"
        self.assertRaises(
            LookupError, m.edit_source_content, undefined_source, type=None
        )

    def test_settingSource(self):
        m = AlternateMedia(type=self.type, length=self.length)
        assert len(m.sources) == 0

        m.sources = self.sources
        assert len(m.sources) == len(self.sources)
        assert "https://example.com/file-0.mp3" in m.sources
        assert m.sources["https://example.com/file-0.mp3"] is None
        assert "ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y" in m.sources
        assert (
            m.sources["ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y"] is None
        )
        assert "https://example.com/file-0.torrent" in m.sources
        assert (
            m.sources["https://example.com/file-0.torrent"]
            == "application/x-bittorrent"
        )

    def test_settingWrongSource(self):
        m = AlternateMedia(type=self.type, length=self.length)
        assert len(m.sources) == 0

        self.assertRaises(TypeError, setattr, m, "sources", object)
        self.assertRaises(TypeError, setattr, m, "sources", 55)
        self.assertRaises(TypeError, setattr, m, "sources", None)
        self.assertRaises(TypeError, setattr, m, "sources", "")
        self.assertRaises(TypeError, setattr, m, "sources", "a valid string")
        self.assertRaises(TypeError, setattr, m, "sources", (1, 2))
        self.assertRaises(TypeError, setattr, m, "sources", [1, 2])

        sources = {
            object: None,
        }
        self.assertRaises(TypeError, setattr, m, "sources", sources)

        sources = {
            55: None,
        }
        self.assertRaises(TypeError, setattr, m, "sources", sources)

        sources = {
            None: None,
        }
        self.assertRaises(TypeError, setattr, m, "sources", sources)

        sources = {
            "not a valid uri": None,
        }
        self.assertRaises(ValueError, setattr, m, "sources", sources)

        sources = {
            "https://example.com/file-0.mp3": object,
        }
        self.assertRaises(TypeError, setattr, m, "sources", sources)

        sources = {
            "https://example.com/file-0.mp3": 55,
        }
        self.assertRaises(TypeError, setattr, m, "sources", sources)

    def test_rss(self):
        m = self._alternate_media()
        element = m.rss_entry()
        assert element.attrib["type"] == self.type
        assert element.attrib["length"] == str(self.length)
        assert float(element.attrib["bitrate"]) == self.bitrate
        assert element.attrib["height"] == str(self.height)
        assert element.attrib["lang"] == self.lang
        assert element.attrib["title"] == self.title
        assert element.attrib["rel"] == self.rel
        assert element.attrib["codecs"] == self.codecs
        assert element.attrib["default"] == self.default_str

        integrity_element = element.find("{%s}integrity" % self.PODCAST_NS)
        assert integrity_element.attrib["value"] == m.signature
        assert integrity_element.attrib["type"] == m.encryption

        sources_copy = self.sources.copy()
        source_elements = element.findall("{%s}source" % self.PODCAST_NS)
        for source_element in source_elements:
            uri = source_element.attrib["uri"]
            content_type = source_element.attrib.get("contentType")
            assert uri in sources_copy
            assert sources_copy[uri] == content_type
            del sources_copy[uri]

        assert sources_copy == {}
