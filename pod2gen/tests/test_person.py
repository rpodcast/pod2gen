# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_person
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Person class, which represents a person or an entity.

    :copyright: 2016, Thorben Dahl <thorben@sjostrom.no>
        2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest

from pod2gen import Person
from pod2gen.warnings import (
    UnknownPersonGroup,
    UnknownPersonGroupRoleTuple,
    UnknownPersonRole,
)


class TestPerson(unittest.TestCase):
    def setUp(self):
        self.name = "Test Person"
        self.email = "test@example.org"
        self.group = "visuals"
        self.role = "cover art designer"
        self.img = "http://example.com/images/alicebrown.jpg"
        self.href = "https://www.wikipedia/alicebrown"

    def _person(self):
        return Person(self.name, self.email, self.group, self.role, self.img, self.href)

    def test_rss(self):
        p = self._person()
        element = p.rss_entry()

        assert element.text == p.name
        assert element.attrib["group"] == self.group
        assert element.attrib["role"] == self.role
        assert element.attrib["img"] == self.img
        assert element.attrib["href"] == self.href

    def test_noName(self):
        p = Person(None, self.email)
        assert p.name is None
        assert p.email == self.email

        element = p.rss_entry()
        assert element.text == self.email

    def test_noEmail(self):
        p = Person(self.name)
        assert p.name == self.name
        assert p.email is None

        element = p.rss_entry()
        assert element.text == self.name

    def test_bothNameAndEmail(self):
        p = Person(self.name, self.email)
        assert p.name == self.name
        assert p.email == self.email

    def test_rssMissingGroup(self):
        p = Person(self.name, self.email, role=self.role, img=self.img, href=self.href)
        element = p.rss_entry()

        assert element.text == self.name
        assert "group" not in element.attrib
        assert element.attrib["role"] == self.role
        assert element.attrib["img"] == self.img
        assert element.attrib["href"] == self.href

    def test_rssMissingRole(self):
        p = Person(
            self.name, self.email, group=self.group, img=self.img, href=self.href
        )
        element = p.rss_entry()

        assert element.text == self.name
        assert element.attrib["group"] == self.group
        assert "role" not in element.attrib
        assert element.attrib["img"] == self.img
        assert element.attrib["href"] == self.href

    def test_rssMissingImg(self):
        p = Person(
            self.name, self.email, group=self.group, role=self.role, href=self.href
        )
        element = p.rss_entry()

        assert element.text == self.name
        assert element.attrib["group"] == self.group
        assert element.attrib["role"] == self.role
        assert "img" not in element.attrib
        assert element.attrib["href"] == self.href

    def test_rssMissingHref(self):
        p = Person(
            self.name, self.email, group=self.group, role=self.role, img=self.img
        )
        element = p.rss_entry()

        assert element.text == self.name
        assert element.attrib["group"] == self.group
        assert element.attrib["role"] == self.role
        assert element.attrib["img"] == self.img
        assert "href" not in element.attrib

    def test_settingName(self):
        other_name = "Mary Sue"
        p = self._person()
        p.name = other_name
        assert p.name == other_name
        assert p.email == self.email

        p.name = None
        assert p.name is None
        assert p.email == self.email

    def test_settingEmail(self):
        other_email = "noreply@example.org"
        p = self._person()
        p.email = other_email
        assert p.name == self.name
        assert p.email == other_email

        p.email = None
        assert p.name == self.name
        assert p.email is None

    def test_settingGroup(self):
        other_group = "new group"
        p = self._person()
        p.group = other_group
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == other_group
        assert p.role == self.role
        assert p.img == self.img
        assert p.href == self.href

        element = p.rss_entry()
        assert element.attrib["group"] == other_group

        p.group = None
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == None
        assert p.role == self.role
        assert p.img == self.img
        assert p.href == self.href

        element = p.rss_entry()
        assert "group" not in element.attrib

    def test_settingRole(self):
        other_role = "new role"
        p = self._person()
        p.role = other_role
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == self.group
        assert p.role == other_role
        assert p.img == self.img
        assert p.href == self.href

        element = p.rss_entry()
        assert element.attrib["role"] == other_role

        p.role = None
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == self.group
        assert p.role == None
        assert p.img == self.img
        assert p.href == self.href

        element = p.rss_entry()
        assert "role" not in element.attrib

    def test_settingImg(self):
        other_img = "http://example.com/images/alicebrown_v2.jpg"
        p = self._person()
        p.img = other_img
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == self.group
        assert p.role == self.role
        assert p.img == other_img
        assert p.href == self.href

        element = p.rss_entry()
        assert element.attrib["img"] == other_img

        p.img = None
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == self.group
        assert p.role == self.role
        assert p.img == None
        assert p.href == self.href

        element = p.rss_entry()
        assert "img" not in element.attrib

    def test_settingHref(self):
        other_href = "https://example.com/artist/bejislim"
        p = self._person()
        p.href = other_href
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == self.group
        assert p.role == self.role
        assert p.img == self.img
        assert p.href == other_href

        element = p.rss_entry()
        assert element.attrib["href"] == other_href

        p.href = None
        assert p.name == self.name
        assert p.email == self.email
        assert p.group == self.group
        assert p.role == self.role
        assert p.img == self.img
        assert p.href == None

        element = p.rss_entry()
        assert "href" not in element.attrib

    def test_invalidConstruction(self):
        self.assertRaises(ValueError, Person)

    def test_invalidSettingName(self):
        p = Person(self.name)
        self.assertRaises(ValueError, setattr, p, "name", None)
        self.assertRaises(ValueError, setattr, p, "name", "a" * 129)
        assert p.name == self.name

    def test_invalidSettingEmail(self):
        p = Person(email=self.email)
        self.assertRaises(ValueError, setattr, p, "email", None)
        assert p.email == self.email

    def test_unknownGroup(self):
        p = self._person()
        self.assertWarns(UnknownPersonGroup, setattr, p, "group", "wrong group")

    def test_unknownRole(self):
        p = self._person()
        self.assertWarns(UnknownPersonRole, setattr, p, "role", "wrong role")

    def test_invalidImg(self):
        self.assertRaises(ValueError, Person, self.name, img="wrong url format")
        self.assertRaises(TypeError, Person, self.name, img=3)

    def test_invalidHref(self):
        self.assertRaises(ValueError, Person, self.name, href="wrong url format")
        self.assertRaises(TypeError, Person, self.name, href=3)

    def test_changingFromNameToMail(self):
        p = Person(name=self.name)
        p.email = self.email
        p.name = None

        assert p.email == self.email
        assert p.name is None

    def test_changingFromMailToName(self):
        p = Person(email=self.email)
        p.name = self.name
        p.email = None

        assert p.name == self.name
        assert p.email is None

    def test_podcatTaxonomyLowercase(self):
        p = self._person()
        for group in p._role_group_taxonomy_mapping:
            assert group.lower() == group
            for role in p._role_group_taxonomy_mapping[group]:
                assert role.lower() == role

    def test_wrongGroupRoleTuple(self):
        p = self._person()
        p.group = "cast"
        p.role = "author"
        self.assertWarns(UnknownPersonGroupRoleTuple, p.rss_entry)
