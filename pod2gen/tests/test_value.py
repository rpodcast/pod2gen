# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_value
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Test the Value class, which represents a value block    
"""
import unittest

from pod2gen import Recipient, Value


class TestValue(unittest.TestCase):
    def setUp(self):
        self.value_type = "lightning"
        self.method = "keysend"
        self.suggested = "0.00000005000"
        self.recipients = [
            Recipient(name="Test Person", address_type="node", address="hhhhhh")
        ]

    def _value(self):
        return Value(
            value_type=self.value_type,
            method=self.method,
            suggested=self.suggested,
            recipients=self.recipients,
        )

    def test_rss(self):
        p = self._value()
        element = p.rss_entry()

        assert element.attrib["type"] == self.value_type
        assert element.attrib["method"] == self.method
        assert element.attrib["suggested"] == self.suggested
