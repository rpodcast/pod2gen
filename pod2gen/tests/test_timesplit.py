# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_timesplit
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Test the TimeSplit class, which represents a value time split in a value block    
"""
import unittest

from pod2gen import TimeSplit, Recipient, RemoteItem

class TestTimeSplit(unittest.TestCase):
    def setUp(self):
        self.startTime = 367
        self.duration = 246
        self.remoteStartTime = 174
        self.remotePercentage = 95
        self.recipients = [
            Recipient(name="Alice (Podcaster)", address_type="node", address="02d5c1bf8b940dc9cadca86d1b0a3c37fbe39cee4c7e839e33bef9174531d27f52", split=85),
            Recipient(name="Jimbob (Guest)", address_type="node", address="032f4ffbbafffbe51726ad3c164a3d0d37ec27bc67b29a159b0f49ae8ac21b8508", split=10)
        ]
        self.remoteitems = [
            RemoteItem(feedGuid="a94f5cc9-8c58-55fc-91fe-a324087a655b", itemGuid="https://podcastindex.org/podcast/4148683#1", medium="music")
        ]

    def _timesplit(self):
        return TimeSplit(
            self.startTime,
            self.duration,
            self.remoteStartTime,
            self.remotePercentage,
            self.recipients
        )

    def test_rss(self):
        p = self._timesplit()
        element = p.rss_entry()

        assert element.attrib["startTime"] == str(self.startTime)
        assert element.attrib["duration"] == str(self.duration)
        assert element.attrib["remoteStartTime"] == str(self.remoteStartTime)
        assert element.attrib["remotePercentage"] == str(self.remotePercentage)
