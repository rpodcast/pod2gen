# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_recipient
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Test the Recipient class, which represents a recipient in a value block    
"""
import unittest

from pod2gen import Recipient


class TestRecipient(unittest.TestCase):
    def setUp(self):
        self.name = "Test Person"
        self.address_type = "node"
        self.address = (
            "02d5c1bf8b940dc9cadca86d1b0a3c37fbe39cee4c7e839e33bef9174531d27f52"
        )
        self.split = 40
        self.customKey = "key1"
        self.customValue = "value1"
        self.fee = True

    def _recipient(self):
        return Recipient(
            self.name,
            self.address_type,
            self.address,
            self.split,
            self.customKey,
            self.customValue,
            self.fee
        )

    def test_rss(self):
        p = self._recipient()
        element = p.rss_entry()

        assert element.attrib["name"] == self.name
        assert element.attrib["type"] == self.address_type
        assert element.attrib["address"] == self.address
        assert element.attrib["split"] == self.split
        assert element.attrib["customKey"] == self.customKey
        assert element.attrib["customValue"] == self.customValue
        assert element.attrib["fee"] == self.fee
