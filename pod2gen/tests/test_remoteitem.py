# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_remoteitem
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Test the RemoteItem class, which represents a remote item block    
"""
import unittest

from pod2gen import RemoteItem

class TestRemoteItem(unittest.TestCase):
    def setUp(self):
        self.feedGuid = "917393e3-1b1e-5cef-ace4-edaa54e1f810"
        self.feedUrl = "https://feeds.example.org/917393e3-1b1e-5cef-ace4-edaa54e1f810/rss.xml"
        self.itemGuid = "asdf089j0-ep240-20230510"
        self.medium = "music"

    def _remoteitem(self):
        return RemoteItem(
            self.feedGuid,
            self.feedUrl,
            self.itemGuid,
            self.medium
        )

    def test_rss(self):
        p = self._remoteitem()
        element = p.rss_entry()

        assert element.attrib["feedGuid"] == self.feedGuid
        assert element.attrib["feedUrl"] == self.feedUrl
        assert element.attrib["itemGuid"] == self.itemGuid
        assert element.attrib["medium"] == self.medium
