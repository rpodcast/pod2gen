# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_license
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the License class, which defines a license that is applied to the
    audio/video content of a single episode, or the audio/video of the
    podcast as a whole.

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest

from pod2gen import License


class TestLicense(unittest.TestCase):
    def setUp(self):
        self.identifier = "cc-by-4.0"
        self.url = "https://example.org/mypodcastlicense/full.pdf"

    def _license(self):
        return License(self.identifier, self.url)

    def test_constructor(self):
        f = License(self.identifier, self.url)
        assert f.identifier == self.identifier
        assert f.url == self.url

    def test_constructorMissingUrl(self):
        l = License(self.identifier)
        assert l.identifier == self.identifier
        assert l.url is None

    def test_settingIdentifier(self):
        other_identifier = "Another identifier"
        l = self._license()
        l.identifier = other_identifier
        assert l.identifier == other_identifier
        assert l.url == self.url

    def test_wrongIdentifier(self):
        self.assertRaises(ValueError, License, 0)
        self.assertRaises(ValueError, License, None)
        self.assertRaises(ValueError, License, "")

    def test_settingUrl(self):
        other_url = "https://example.org/mypodcastlicense_v2/full.pdf"
        l = self._license()
        l.url = other_url
        assert l.identifier == self.identifier
        assert l.url == other_url

    def test_wrongUrl(self):
        self.assertRaises(ValueError, License, self.identifier, "Not an actual url")
