# -*- coding: utf-8 -*-
"""
    pod2gen.remoteitem
    ~~~~~~~~~~~~~~~~~

    This file contains the RemoteItem class, which is used to represent a
    remote item block.
"""

import warnings

from lxml import etree

class RemoteItem(object):
    def __init__(
        self,
        feedGuid,
        feedUrl=None,
        itemGuid=None,
        medium=None
    ):
        """Create new remote item"""
        if not self._is_valid(feedGuid):
            raise ValueError("You must provide a feed guid.")
        
        self.__feedGuid = feedGuid
        self.__feedUrl = feedUrl
        self.__itemGuid = itemGuid
        self.__medium = medium

    def _is_valid(self, feedGuid):
        return feedGuid
    
    def rss_entry(self):
        PODCAST_NS = "https://podcastindex.org/namespace/1.0"
        entry = etree.Element("{%s}remoteItem" % PODCAST_NS)

        entry.attrib["feedGuid"] = self.feedGuid
        
        if self.feedUrl:
            entry.attrib["feedUrl"] = self.feedUrl

        if self.itemGuid:
            entry.attrib["itemGuid"] = self.itemGuid
        
        if self.medium:
            entry.attrib["medium"] = self.medium

        return entry
    
    @property
    def feedGuid(self):
        """The podcast GUID of the remote feed being pointed to
        
        :type: :obj:`str`
        """
        return self.__feedGuid
    
    @feedGuid.setter
    def feedGuid(self, new_feedGuid):
        self.__feedGuid = new_feedGuid

    @property
    def feedUrl(self):
        """The URL of the remote feed being pointed to
        
        "type: :obj:`str`
        """
        return self.__feedUrl
    
    @feedUrl.setter
    def feedUrl(self, new_feedUrl):
        self.__feedUrl = new_feedUrl

    @property
    def itemGuid(self):
        """If the feed being pointed to is intended to point
        to an item in the remote feed, this attribute should
        contain the value of the guid of the item
        
        :type: :obj"`str`
        """
        return self.__itemGuid
    
    @itemGuid.setter
    def itemGuid(self, new_itemGuid):
        self.__itemGuid = new_itemGuid

    @property
    def medium(self):
        """If the feed being pointed to is not of medium type
        'podcast', this attribute should contain it's podcast
        medium type.
        
        :type: :obj:`str`
        """
        return self.__medium
    
    @medium.setter
    def medium(self, new_medium):
        self.__medium = new_medium

    def __str__(self):
        return "%s" % (self.feedGuid)
    
    def __repr__(self):
        return "RemoteItem(feedGuid=%s)" % (self.feedGuid)


