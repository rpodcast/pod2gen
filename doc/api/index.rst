=================
API Documentation
=================

.. autosummary::

   ~pod2gen.Podcast
   ~pod2gen.Episode
   ~pod2gen.Person
   ~pod2gen.Media
   ~pod2gen.AlternateMedia
   ~pod2gen.Category
   ~pod2gen.Funding
   ~pod2gen.License
   ~pod2gen.Location
   ~pod2gen.Soundbite
   ~pod2gen.Trailer
   ~pod2gen.Transcript
   ~pod2gen.warnings
   ~pod2gen.util

.. toctree::
   :maxdepth: 1
   :hidden:

   podcast
   episode
   person
   media
   alternate_media
   category
   funding
   license
   location
   soundbite
   trailer
   transcript
   warnings
   util
