============
Full example
============

This example is located at ``pod2gen/__main__.py`` in the package, and is run
as part of the :doc:`testing routines </contributing>`.

.. literalinclude:: ../../pod2gen/__main__.py
   :pyobject: main
   :linenos:

