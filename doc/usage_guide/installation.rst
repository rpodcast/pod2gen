============
Installation
============

pod2gen can be used on any system (if not: file a bug report!), and officially supports
Python 3.6+

Use `pip <https://pypi.python.org/pypi>`_::

    $ pip install pod2gen

Remember to use a `virtual environment <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_!
