Podcasts
--------

In pod2gen, the term *podcast* refers to the show which listeners can subscribe to,
which consists of individual *episodes*. Therefore, the Podcast class will be the
first thing you start with.

Creating a new instance
~~~~~~~~~~~~~~~~~~~~~~~

You can start with a blank podcast by invoking the Podcast constructor with no
arguments, like this::

    from pod2gen import Podcast
    p = Podcast()

Mandatory attributes
~~~~~~~~~~~~~~~~~~~~

There are four attributes which must be set before you can generate your podcast.
They are mandatory because Apple's podcast directory will not accept podcasts without
this information. If you try to generate the podcast without setting all of the
mandatory attributes, you will get an error.

The mandatory attributes are::

    p.name = "My Example Podcast"
    p.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    p.website = "https://example.org"
    p.explicit = False

They're mostly self explanatory, but you can read more about them if you'd like:

* :attr:`~pod2gen.Podcast.name`
* :attr:`~pod2gen.Podcast.description`
* :attr:`~pod2gen.Podcast.website`
* :attr:`~pod2gen.Podcast.explicit`

Image
~~~~~

A podcast's image is worth special attention::

    p.image = "https://example.com/static/example_podcast.png"

.. autoattribute:: pod2gen.Podcast.image
   :noindex:

Even though the image *technically* is optional, you won't reach people without it.


The two types of podcasts
~~~~~~~~~~~~~~~~~~~~~~~~~

There are two types of podcasts in the world (according to Apple Podcasts, anyway):

* **Episodic** podcasts are podcasts whose episodes are meant to be consumed in any order.
  New listeners will likely start with the newest episode.
  This is the traditional type of podcast, with examples like "The Daily" and "The Joe Rogan Experience".
  Each episode of an episodic podcast is largely self-contained, although there may be
  recurring jokes and references to older episodes.

* **Serial** podcasts are podcasts whose episodes must be consumed from beginning to end.
  New listeners will likely start with the first episode of the current season.
  This is a newer phenomenon, made popular by the appropriately titled podcast "Serial".
  Each episode of a serial podcast starts off where the last episode ended, though the seasons
  are independent from one another.

If you don't do anything, pod2gen will assume that your podcast is episodic.

If your podcast is serial, you can set the :attr:`~pod2gen.Podcast.is_serial` attribute to :data:`True`, like this::

   p.is_serial = True

.. note::

   When :attr:`~pod2gen.Podcast.is_serial` is set to :data:`True`,
   all full episodes must be given an
   :attr:`episode_number <pod2gen.Episode.episode_number>`. Additionally, it is
   recommended that you associate each episode with a season. This is covered on
   the :doc:`Episode <episodes>` page.


Optional attributes
~~~~~~~~~~~~~~~~~~~

There are plenty of other attributes that can be used with
:class:`~pod2gen.Podcast`:


Commonly used
^^^^^^^^^^^^^

::

    p.copyright = "2016 Example Radio"
    p.language = "en-US"
    p.authors = [Person("John Doe", "editor@example.org")]
    p.feed_url = "https://example.com/feeds/podcast.rss"  # URL of this feed
    p.category = Category("Music", "Music History")
    p.owner = p.authors[0]
    p.xslt = "https://example.com/feed/stylesheet.xsl"  # URL of XSLT stylesheet

Read more:

* :attr:`~pod2gen.Podcast.copyright`
* :attr:`~pod2gen.Podcast.language`
* :attr:`~pod2gen.Podcast.authors`
* :attr:`~pod2gen.Podcast.feed_url`
* :attr:`~pod2gen.Podcast.category`
* :attr:`~pod2gen.Podcast.owner`
* :attr:`~pod2gen.Podcast.xslt`


Podcastindex related attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The `RSS Namespace Extension for Podcasting`_ introduced several new XML tags
related to the world of podcasting. These new attributes meant to synthesize
the fragmented world of podcast namespaces.

Locked
==================

The purpose of the :attr:`~pod2gen.Podcast.locked` attribute is to tell other
podcast platforms whether they are allowed to import this feed or not.

::

    # The podcast feed can be imported by any podcast platform.
    p.locked = False

    # Importing the podcast feed is not allowed
    p.locked = True

Read more:

* :attr:`Podcast.locked <pod2gen.Podcast.locked>`


Fundings
==================

:attr:`Podcast.fundings <pod2gen.Podcast.fundings>` contains a list of
:class:`~pod2gen.Funding` objects representing possible donation/funding links.
Use :meth:`Podcast.add_funding() <pod2gen.Podcast.add_funding>`
to add a Funding object.

::

    from pod2gen import Funding
    funding = Funding("Support the show!", "https://www.example.com/donations")
    p.add_funding(funding)

Read more:

* :class:`~pod2gen.Funding`
* :attr:`Podcast.fundings <pod2gen.Podcast.fundings>`
* :meth:`Podcast.add_funding() <pod2gen.Podcast.add_funding>`


Persons
==================

:attr:`Podcast.persons <pod2gen.Podcast.persons>` contains a list of
:class:`~pod2gen.Person` objects representing persons of interest to the
podcast. Although, it is flexible enough to allow fuller credits to be
given using the roles and groups that are listed in the
`Podcast Taxonomy Project`_.
Use :meth:`Podcast.add_person() <pod2gen.Podcast.add_person>`
to add a Person object

::

    from pod2gen import Person
    person = Person(
        email="mslimbeji@gmail.com",
        group="writing",
        role="guest writer",
        href="https://github.com/SlimBeji",
        img="http://example.com/images/slimbeji.jpg",
    )
    p.add_person(person)

Read more:

* :class:`~pod2gen.Person`
* :attr:`Podcast.persons <pod2gen.Podcast.persons>`
* :meth:`Podcast.add_person() <pod2gen.Podcast.add_person>`


Location
==================

The purpose of the :attr:`~pod2gen.Podcast.location` attribute is to to describe
the location of editorial focus for a podcast's content
(i.e. "what place is this podcast about?").

::

    from pod2gen import Location
    p.location = Location(
        "Dreamworld (Queensland)", geo="geo:-27.86159,153.3169", osm="W43678282"
    )

Read more:

* :class:`~pod2gen.Location`
* :attr:`Podcast.location <pod2gen.Podcast.location>`


Trailer
==================

:attr:`Podcast.trailers <pod2gen.Podcast.trailers>` contains a list of
:class:`~pod2gen.Trailer` objects representing audio or video files to
be used as trailers. Use
:meth:`Podcast.add_trailer() <pod2gen.Podcast.add_trailer>` to add a
trailer.

::

    from pod2gen import Trailer
    trailer = Trailer(
        text="Coming April 1st, 2021",
        url="https://example.org/trailers/teaser",
        pubdate=datetime.datetime(2021, 8, 15, 8, 15, 12, 0, tz.UTC),
        length=12345678,
        type="audio/mp3",
        season=2,
    )
    p.add_trailer(trailer)

Read more:

* :class:`~pod2gen.Trailer`
* :attr:`Podcast.trailers <pod2gen.Podcast.trailers>`
* :meth:`Podcast.add_trailer() <pod2gen.Podcast.add_trailer>`


License
==================

The purpose of the :attr:`~pod2gen.Podcast.license` attribute is to define a
license that is applied to the audio/video content of the podcast as a whole.

::

    from pod2gen import License
    p.license = License(
        "my-podcast-license-v1", "https://example.org/mypodcastlicense/full.pdf"
    )

Read more:

* :class:`~pod2gen.License`
* :attr:`Podcast.license <pod2gen.Podcast.license>`


GUID
==================

The purpose of the :attr:`~pod2gen.Podcast.guid` attribute is to declare a unique,
global identifier for a podcast. It must follow a certain format, thus, the method
:meth:`Podcast.generate_guid() <pod2gen.Podcast.generate_guid>` should be used

::

    p.generate_guid()

Read more:

* :attr:`Podcast.guid <pod2gen.Podcast.guid>`
* :meth:`Podcast.generate_guid() <pod2gen.Podcast.generate_guid>`


Less commonly used
^^^^^^^^^^^^^^^^^^

Some of those are obscure while some of them are often times not needed. Others
again have very reasonable defaults.

::

    # RSS Cloud enables podcatchers to subscribe to notifications when there's
    # a new episode ready, however it's not used much.
    p.cloud = ("server.example.com", 80, "/rpc", "cloud.notify", "xml-rpc")

    import datetime
    from dateutil import tz
    # last_updated is datetime when the feed was last refreshed. If you don't
    # set it, the current date and time will be used instead when the feed is
    # generated, which is generally what you want. Nevertheless, you can
    # set your own date:
    p.last_updated = datetime.datetime(2016, 5, 18, 0, 0, tzinfo=tz.UTC))

    # publication_date is when the contents of this feed last were published.
    # If you don't set it, the date of the most recent Episode is used. Again,
    # this is generally what you want, but you can override it:
    p.publication_date = datetime.datetime(2016, 5, 17, 15, 32,tzinfo=tz.UTC))

    # Set of days on which podcatchers won't need to refresh the feed.
    # Not implemented widely.
    p.skip_days = {"Friday", "Saturday", "Sunday"}

    # Set of hours on which podcatchers won't need to refresh the feed.
    # Not implemented widely.
    p.skip_hours = set(range(8))
    p.skip_hours |= set(range(16, 24))

    # Person to contact regarding technical aspects of the feed.
    p.web_master = Person(None, "helpdesk@dallas.example.com")

    # Identify the software which generates the feed (defaults to pod2gen)
    p.set_generator("ExamplePodcastProgram", (1,0,0))
    # (you can also set the generator string directly)
    p.generator = "ExamplePodcastProgram v1.0.0 (with help from python-feedgen)"

    # !!! Be very careful about using the following attributes !!!

    # Tell iTunes that this feed has moved somewhere else.
    p.new_feed_url = "https://podcast.example.com/example"

    # Tell iTunes that this feed will never be updated again.
    p.complete = True

    # Tell iTunes that you'd rather not have this feed appear on iTunes.
    p.withhold_from_itunes = True

Read more:

* :attr:`~pod2gen.Podcast.cloud`
* :attr:`~pod2gen.Podcast.last_updated`
* :attr:`~pod2gen.Podcast.publication_date`
* :attr:`~pod2gen.Podcast.skip_days`
* :attr:`~pod2gen.Podcast.skip_hours`
* :attr:`~pod2gen.Podcast.web_master`
* :meth:`~pod2gen.Podcast.set_generator`
* :attr:`~pod2gen.Podcast.new_feed_url`
* :attr:`~pod2gen.Podcast.complete`
* :attr:`~pod2gen.Podcast.withhold_from_itunes`

Shortcut for filling in data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead of creating a new :class:`.Podcast` object in one statement, and
populating it with data one statement at a time afterwards, you can create a
new :class:`.Podcast` object and fill it with data in one statement. Simply
use the attribute name as keyword arguments to the constructor::

   import pod2gen
   p = pod2gen.Podcast(
       <attribute name>=<attribute value>,
       <attribute name>=<attribute value>,
       ...
   )

Using this technique, you can define the Podcast as part of a list
comprehension, dictionaries and so on.
Take a look at the :doc:`API Documentation for Podcast </api/podcast>` for a
practical example.

.. _`RSS Namespace Extension for Podcasting`: https://podcastindex.org/namespace/1.0
.. _`Podcast Taxonomy Project`: https://podcasttaxonomy.com/home