-------
License
-------
pod2gen is licensed under the terms of both the FreeBSD license and the LGPLv3+.
Choose the one which is more convenient for you. For more details, have a look
at license.bsd_ and license.lgpl_.

.. _license.bsd: https://gitlab.com/caproni-podcast-publishing/pod2gen/-/blob/master/license.bsd
.. _license.lgpl: https://gitlab.com/caproni-podcast-publishing/pod2gen/-/blob/master/license.lgpl
