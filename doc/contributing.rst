============
Contributing
============

Setting up
----------

To install the dependencies, run::

    $ pip install -r requirements.txt

while you have a `virtual environment <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_
activated.

You are recommended to use `pyenv <https://github.com/yyuu/pyenv>`_ to handle
virtual environments and Python versions. That way, you can easily test and
debug problems that are specific to one version of Python.

Testing
-------

You can perform an integration test by running ``pod2gen/__main__.py``::

    $ python -m pod2gen

When working on this project, you should run the unit tests as well as the
integration test, like this::

    $ make test

The unit tests reside in ``pod2gen/tests`` and are written using the
:mod:`unittest` module.


Values
------

Read :doc:`/background/philosophy`, :doc:`/background/scope` and :doc:`/background/fork`
for a run-down on what values/principles lay the foundation for this project.
In short, it is important to keep the API as simple as possible.

You must also write unittests as you code, ideally using **test-driven
development** (that is, write a test, observe that the test fails, write code
so the test works, observe that the test succeeds, write a new test and so on).
That way, you know that the tests actually contribute and you get to think
about how the API will look before you tackle the problem head-on.

Make sure you update ``pod2gen/__main__.py`` so it still works, and use your new
functionality there if it makes sense.

You must also make sure you **update any relevant documentation**. Remember that
the documentation includes lots of examples and also describes the API
independently from docstring comments in the code itself.

Pull requests in which the unittests and documentation are NOT up to date
with the code will NOT be accepted.

Lastly, a single **commit** shouldn't include more changes than it needs. It's better to do a big
change in small steps, each of which is one commit. Explain the impact of your
changes in the commit message.

The Workflow
------------

#. Check out our `GitLab Issues <https://gitlab.com/caproni-podcast-publishing/pod2gen/-/issues>`_.

   * Find the issue you wish to work on.
   * Add your issue if it's not already there.
   * Discuss the issue and get feedback on your proposed solution. Don't waste
     time on a solution that might not be accepted!

#. Work on the issue in a separate branch which follows the name scheme
   ``#<issue-number>-<brief-description>`` in your own fork.

#. Push the branch.

#. Do the work.

#. When you're done and you've updated the documentation and tests (see above),
   create a pull request which references the issue.

#. Wait for the pull request to be reviewed. Keep an eye on your inbox or your
   GitLab notifications, in case of objections or feedback that must be taken
   into consideration.

Linting
---------

pod2gen uses `Black <https://black.readthedocs.io/en/stable/>`_ (version 21.8b0) for
linting and `Isort <https://pycqa.github.io/isort/>`_ (version 5.9.3) for organizing
python imports.

It is import to use the mentioned versions above to make sure that the code
formatting stay coherent. Please make sure to always lint your code using isort
and black before committing code.

In order to make sure that black and isort work well together, a .isort.cfg file
is added to the root directory with the following content:

::

    [settings]
    profile=black
